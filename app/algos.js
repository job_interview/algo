module.exports.increment = (numberArray) => {
  let retenue = true;
  for (idx = numberArray.length - 1; idx >= 0; idx--) {
    if (!retenue) break;
    const digit = ++numberArray[idx];
    if (digit < 10) {
      retenue = false;
      break;
    }
    numberArray[idx] = numberArray[idx] - 10;
    retenue = true;
  }
  if (retenue) numberArray.unshift(1); //this can involve a full copy of the array, but it's not supposed to occurs every time
  return numberArray;
};

module.exports.fizzBuzz = (N, log) => {
  console.log("<=", N);
  let modulo3 = 1;
  let modulo5 = 1;
  let number = 1;
  while (number <= N) {
    const isModulo3 = modulo3 === 3;
    const isModulo5 = modulo5 === 5;

    if (isModulo3) modulo3 = 0;
    if (isModulo5) modulo5 = 0;

    if (isModulo3 && isModulo5) log("FizzBuzz");
    else if (isModulo3) log("Fizz");
    else if (isModulo5) log("Buzz");
    else log(number);

    modulo3++;
    modulo5++;
    number++;
  }
};
