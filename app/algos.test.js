const algos = require("./algos");

describe("Algos", function () {
  it("increment without holdback", async () => {
    const result = algos.increment([1, 2, 3]);
    expect(result).toEqual([1, 2, 4]);
  });

  it("increment wit holdback 1", async () => {
    const result = algos.increment([1, 2, 9]);
    expect(result).toEqual([1, 3, 0]);
  });

  it("increment wit holdback 3", async () => {
    const result = algos.increment([5, 9, 9]);
    expect(result).toEqual([6, 0, 0]);
  });

  it("increment wit holdback 4", async () => {
    const result = algos.increment([9, 9, 9]);
    expect(result).toEqual([1, 0, 0, 0]);
  });

  it("fizzBuzz", async () => {
    const array = [];
    algos.fizzBuzz(19, array.push.bind(array));
    expect(array).toEqual([
      1,
      2,
      "Fizz",
      4,
      "Buzz",
      "Fizz",
      7,
      8,
      "Fizz",
      "Buzz",
      11,
      "Fizz",
      13,
      14,
      "FizzBuzz",
      16,
      17,
      "Fizz",
      19,
    ]);
  });
});
